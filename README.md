# Software Developer Coding Challenge: COVID-19 PCR test api and client.

`Version 2.0 is available at:` https://gitlab.com/seyit/arzt/-/tree/v2

----

This project executes the tasks specified in the coding challenge using the Flask, Mongo and Vuejs stack. Docker was used for the distribution of the project. The project is not ready for production deployment. Services work with the development/debug configuration at the moment.

1. It would be more appropriate to use **uwsgi** for the production deployment of the _backend (Flask)_ application.
2. The _frontend (Vuejs)_ application runs with `npm run serve`. It is necessary to use `npm run build` for production and it would be appropriate to serve static files with **http-server**.
3. **Nginx** should also be used for production deployment, for this it would be appropriate to run as a separate service in docker-compose with the necessary config.

### Project Requirements
`Docker, Docker Compose`

### Installation
Since all services are defined in docker-compose, they can be run with the following command after the **environment variables** below are defined.

Since Nginx is not used and services are running in development mode, they can be accessed via default ports. Before deploying, it should be checked that there are no other services running on the same ports.

`docker-compose -f docker-compose.yml up`

Since Nginx is not used and services are running in development mode, they can be accessed via default ports. Before deploying, it should be checked that there are no other services running on the same ports.

- User Interface (frontend): `http://127.0.0.1:8080/`
- API (backend): `http://127.0.0.1:5000/api`

Required environment variable. (Optionally default values can also be used)
```
backend 
- MONGO_URI=mongodb://mongodb:27017/testdb
- MAIL_SERVER="smtp.mailtrap.io"
- MAIL_PORT=2525
- MAIL_USERNAME=""
- MAIL_PASSWORD=""
- MAIL_USE_TLS=False
- MAIL_FROM='test@test.com'

mongodb
- MONGO_INITDB_DATABASE="testdb"
```

## Notes for Task 1

**OpenAPI** standards and **Swagger** were used in writing the API specification.

**The Bearer token** is defined in **securitySchemes** in the specification and as stated in the challenge document, this token must be specified in the Header for communication with the two endpoints, but it is used for sample specific access, not User Authentication.

**API documentation**: [api_specifications.yml](api_specifications.yml)

## Notes for Task 2

Backend service and restful API is developed with **Flask**. Also **MongoDB** is used as database with **flask_mongoengine** for DB models and communication.

API communicates over three endpoints as described in the specification.

```
/api/submit_sample
/api/update_sample
/api/get_sample
```

There is a simple `/api/check` endpoint to check that the project is up and running.

Since the Bearer token is used sample specific like an identifier, integrations such as session auth or login, logout have not been made. The token is encoded with unique information belonging to the sample and decoded by reading from the request header to understand which sample is accessed, with necessary controls and validations. 

There is a certain level of validation and input/output control in API queries, but it would be appropriate to use more comprehensive tools for serialization/deserialization. e.g. **Marshmallow**

Only static functions are tested. There are no tests for endpoints or parts that need application object.

For further development of the project, it would be appropriate to refractor it to the **Flask Application Factories** structure. In its current state, there may be problems in accessing the app context of the modules or circular imports.

CORS config is currently used with wild-card (*). It will be safer to specify the relevant domains in the production setup.

Backend application (API) can also be run as a standalone Flask project ([Dockerfile](backend/Dockerfile)). For this it needs **Environment Variables** and a running **MongoDB**. 

## Notes for Task 3

**Celery** and **redis (or rabbitmq)** will be more reliable in executing this task, but due to the current architecture of the project, it was difficult to integrate it due to circular imports and app context issues. **Asyncio** and **aiosmtplib** have been used instead, but this is not functionally sufficient.

## Notes for Task 4

**Vuejs*** is used for frontend service. The project structure was created with the **Vue CLI**. **Vuetify** is used as UI library. **Axios** is used for API communication.

A clear and understandable interface is aimed to fulfill the functions specified in the API specification.

**api_domain** is specified as hardcoded from the **HomePage** component. Based on deployment and maintainability it can be read from the environment or specified in the config.

Since username and password are not kept for users, there is no login screen in the project. As a single page app, all functions are given on a single screen on the same page. In the actual use of the system, Medical Doctor and Lab Tecnicians should not see each other's screens and should not be able to perform operations for each other.

Since the bearer token is defined for sample specific access only, token information is not kept in the session. The token must be provided by the user as it is the identifier of the sample.

![UI](public/user-interface.png?raw=true "User Interface")

