from enum import Enum
from db import db


class Status(Enum):
    COMPLETED = 'completed'
    FAILED = 'failed'

class Result(Enum):
    POSITIVE = 'positive'
    NEGATIVE = 'negative'


class Sample(db.Document):
    sample_id = db.SequenceField(required=True, primary_key=True)
    patient_pseudonym = db.StringField(min_length=10, max_length=64, required=True)
    submitter_email = db.EmailField(required=True)
    collection_date = db.StringField(required=True)
    status = db.EnumField(Status)
    test_result = db.EnumField(Result)
    test_date = db.StringField()
    meta = {
        'indexes': [
            {'fields': ('patient_pseudonym', 'submitter_email', 'collection_date'), 'unique': True}
        ]
    }
