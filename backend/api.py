import json

from flask import Response, current_app, request
from flask_restful import Resource
from jwt import DecodeError
from mongoengine import NotUniqueError, ValidationError, InvalidQueryError

from helpers import create_token, decode_token
from models import Sample, Status
from flask_restful import reqparse

from mail import sent_mail


class Check(Resource):
    def get(self):
        return {"message": "success"}, 200


class SubmitSample(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('patient_pseudonym', type=str, help='swagger: patient_pseudonym', location='json')
        parser.add_argument('submitter_email', type=str, help='swagger: submitter_email', location='json')
        parser.add_argument('collection_date', type=str, help='swagger: collection_date', location='json')

        args = parser.parse_args()

        # Validation can also be done with the serializer before the data is sent to the db model.
        try:
            sample = Sample(**args).save()
        except NotUniqueError as e:
            return {'message': 'duplicate entry'}, 409
        except ValidationError as e:
            return {'message': str(e)}, 400

        token = create_token(args, current_app.config.get('SECRET'))

        id = sample.id
        return {'sample_id': str(id), 'access_token': str(token)}, 200


class UpdateSample(Resource):
    def put(self):

        auth_header = request.headers.get('Authorization')

        try:
            token_data = decode_token(auth_header, current_app.config.get('SECRET'))
        except (DecodeError, ValueError) as error:
            return str(error), 403

        parser = reqparse.RequestParser()
        parser.add_argument('status', type=str, help='swagger: status', location='json')
        parser.add_argument('test_result', type=str, help='swagger: test_result', location='json')
        parser.add_argument('test_date', type=str, help='swagger: test_date', location='json')

        args = parser.parse_args()

        if args.get("status") == Status.COMPLETED.value:
            if (args.get("test_date") is None) or (args.get("test_result") is None):
                return {'message': 'Please include test date and test result for completed tests.'}, 400

        try:
            sample = Sample.objects(**token_data).first()
        except InvalidQueryError as e:
            return {'message': 'Invalid or wrong token'}, 400
        else:
            if not sample:
                return {'message': 'Sample not found'}, 404

        # Validation can also be done with the serializer before the data is sent to the db model.
        try:
            sample.update(**args)
            sample.save()
        except ValidationError as e:
            return {'message': str(e)}, 400
        else:
            # db update is okay, notify user with simple, async email message
            try:
            # this is a bad practice; try/except used because any error causes the request fail
                email_data = {
                    'subject': 'Sample data updated',
                    'to': sample.submitter_email,
                    'body': f'Status of {sample.patient_pseudonym} sample has been updated as {sample.status}.'
                }
                sent_mail(email_data)
            except Exception as e:
                current_app.logger.error(f"Error sending email. {e}")

        return {}, 204


class GetSample(Resource):
    def get(self):

        auth_header = request.headers.get('Authorization')

        try:
            token_data = decode_token(auth_header, current_app.config.get('SECRET'))
        except (DecodeError, ValueError) as error:
            return str(error), 403

        sample = Sample.objects(**token_data).first()

        # alternative: serializer
        resp = {
            "sample_id": sample.id,
            "patient_pseudonym": sample.patient_pseudonym,
            "submitter_email": sample.submitter_email,
            "collection_date": sample.collection_date,
            "status": sample.status.value if sample.status else None,
        }
        if sample.status:
            if sample.status.value == Status.COMPLETED.value:
                resp.update({"test_result": sample.test_result.value, "test_date": sample.test_date})

        return resp, 200


def initialize_api(api):
    api.add_resource(Check, '/api/check')
    api.add_resource(SubmitSample, '/api/submit_sample')
    api.add_resource(UpdateSample, '/api/update_sample')
    api.add_resource(GetSample, '/api/get_sample')
