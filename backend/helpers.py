import jwt

from threading import Thread


def threading(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()

    return wrapper


def create_token(payload, secret):
    """Create JWT token"""
    return jwt.encode(payload, secret, algorithm="HS256")


def decode_token(auth_header, secret):
    """Parse JWT token from authorization header and decode"""
    from jwt import DecodeError
    PREFIX = 'Bearer '
    if auth_header and auth_header.startswith(PREFIX):
        token = auth_header.replace(PREFIX, '')
        try:
            return jwt.decode(token, secret, algorithms="HS256")
        except DecodeError as e:
            raise e
    else:
        raise ValueError(f"Bearer token not found in auth header")
