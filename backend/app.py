import os

from flask import Flask

from flask_restful import Api
from flask_cors import CORS

from db import initialize_db
from api import initialize_api

app = Flask(__name__)
app.config['SECRET'] = os.getenv("SECRET", "Um8VC1f24cpHO38Jr4jjNA")
app.config['MONGODB_SETTINGS'] = {"host": os.getenv("MONGO_URI", "mongodb://localhost:27017/defaultdb")}

app.config['MAIL_SERVER'] = "smtp.mailtrap.io"
app.config['MAIL_PORT'] = 2525
app.config['MAIL_USERNAME'] = "87c93340b32970"
app.config['MAIL_PASSWORD'] = "08fad56a1f06ec"
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_FROM'] = 'seyit@test.com'

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

initialize_db(app)

api = Api(app)
initialize_api(api)


if __name__ == '__main__':
    app.run(debug=True)
