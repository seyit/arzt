from flask_mongoengine import MongoEngine

db = MongoEngine()


def initialize_db(flaskapp):
    db.init_app(flaskapp)
