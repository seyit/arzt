import asyncio
from email.message import EmailMessage

import aiosmtplib
from flask import current_app

# this should be celery task for better performance
async def send_async_email(email_data):
    message = EmailMessage()
    message["From"] = "seyit@test.com"
    message["To"] = email_data["to"]
    message["Subject"] = email_data["subject"]
    message.set_content(email_data["body"])

    await aiosmtplib.send(
        message,
        hostname=current_app.config['MAIL_SERVER'],
        port=int(current_app.config['MAIL_PORT']),
        username=current_app.config['MAIL_USERNAME'],
        password=current_app.config['MAIL_PASSWORD'],
        use_tls=current_app.config['MAIL_USE_TLS']
    )

def sent_mail(email_data):
    event_loop = asyncio.new_event_loop()
    asyncio.set_event_loop(event_loop)
    try:
        event_loop.run_until_complete(send_async_email(email_data))
    except Exception as e:
        current_app.logger.error(f"Error sending email. {e}")
    finally:
        event_loop.close()


