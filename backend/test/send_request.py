import requests

auth_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXRpZW50X3BzZXVkb255bSI6InZwYXRpZW50X3BzZXVkb255bSIsInN1Ym1pdHRlcl9lbWFpbCI6InRlc3RAdGVzdC5jb20iLCJjb2xsZWN0aW9uX2RhdGUiOiIyMDIyLTA4LTE4VDE3OjEyOjQ0LjIzNFoifQ.oQcZHTlgnQP741wk6GgNEWl3n7KQUXnFFgfEjqxvERU'

hed = {'Authorization': 'Bearer ' + auth_token}

url = 'http://127.0.0.1:5000/api/update_sample'
data = {
    "status": "failed",
}
response = requests.put(url, json=data, headers=hed)

print(response)

print(response.json())
