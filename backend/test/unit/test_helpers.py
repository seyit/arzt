import unittest


class TokenTest(unittest.TestCase):
    test_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0ZXN0IjoiY2FzZTEifQ.7EE3LJzU8LBr08nD__ZDplHvNKVy-PDjbJ9GNBCdEmw"
    broken_token = "brokentoken"
    secret = 'ubFtq8XsuyyZQHtcHTlZcQ'
    payload = {'test': 'case1'}

    def test_encode_token(self):
        from jwt import DecodeError
        from helpers import decode_token

        # case 1
        content = decode_token(f"Bearer {self.test_token}", self.secret)
        self.assertEqual(content, self.payload)

        # case 2: broken token
        with self.assertRaises(DecodeError):
            decode_token(f"Bearer {self.broken_token}", self.secret)

        # case 2: broken header
        with self.assertRaises(ValueError):
            decode_token(f"Broken {self.broken_token}", self.secret)

    def test_create_token(self):
        from helpers import create_token

        # case 1
        created_token = create_token(self.payload, self.secret)
        self.assertEqual(created_token, self.test_token)


if __name__ == '__main__':
    unittest.main()
